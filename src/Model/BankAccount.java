package Model;

public class BankAccount {
	private double balance;
	
	public void deposit(String amount){
		double moneyIn = Double.parseDouble(amount);
		balance += moneyIn;
	}
	
	public void withdraw(String amount){
		double moneyOut = Double.parseDouble(amount);
		balance -= moneyOut;
	}
	
	public String getBalance(){
		return balance+"";
	}
}
